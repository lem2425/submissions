def caro(índice_medicamentos):
    mais_caro = None
    for item in índice_medicamentos:
        if mais_caro is None or item["Preço"] > mais_caro["Preço"]:
            mais_caro = item
    return mais_caro["Nome"]

def caro2(índice_medicamentos):
    mais_caro = None
    for item in índice_medicamentos:
        if mais_caro is None or item["Preço"] > mais_caro["Preço"]:
            mais_caro = item
    return mais_caro["Preço"]

def stock(índice_medicamentos):
    semstock = []
    for item in índice_medicamentos:
        if item["Quantidade"] == 0:
            semstock.append(item)
    nomes_sem_stock = [item["Nome"] for item in semstock]
    return nomes_sem_stock

def total(índice_medicamentos):
    total = 0
    for item in índice_medicamentos:
        total = total + item["Preço"]
    return total

def tamanho(índice_medicamentos):
    tam=0
    for item in índice_medicamentos:
        tam=tam+item["Quantidade"]
    return tam

print("Introduza o nome, o laboratório de origem, a quantidade em stock e o preço de venda de 10 medicamentos diferentes")
i = 0
medicamentos = []
while i < 10:
    print("Nome do medicamento:")
    nome = input()
    print("Laboratório de origem:")
    lab = input()
    while True:
        print("Quantidade em stock:")
        quant_input = input()
        if quant_input.isdigit():
            quant = int(quant_input)
            break
        else:
            print("Quantidade inválida. Digite um número inteiro.")
    while True:
        print("Preço:")
        preco_input = input()
        if preco_input.isdigit():
            preco = float(preco_input)
            break
        else:
            print("Quantidade inválida. Digite um número inteiro.")
    i = i + 1
    
    medicamento = {
        "Nome": nome,
        "laboratorio de origem": lab,
        "Quantidade em stock": quant,
        "Preço": preco}
    medicamentos.append(medicamento)
print("")
mais_caro = caro(medicamentos)
maiscaro = caro2(medicamentos)
if mais_caro is not None:
    print("Medicamento mais caro:")
    print("O medicamento mais caro é ",mais_caro, " e o seu custo é ",maiscaro," euros.")
    
else:
    print("Não foram introduzidos medicamentos.")
print("")
sem_stock = stock(medicamentos)
print("")
print("Os medicamentos sem stock são:")
for nome in sem_stock:
    print(nome)
print("")
custo=total(medicamentos)
print("O custo total é ",custo)
print("")
tamanho_total=tamanho(medicamentos)
a=15*tamanho_total
b=10*tamanho_total 
c=7*tamanho_total
print("O espaço total necessário para armazenar todo o stock é (",a," x ",b," x ",c,"). ")